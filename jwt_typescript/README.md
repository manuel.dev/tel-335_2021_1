# Pasos para ejecutar docker:

```
# docker build -t tel335-jwt-docker .
# docker run -p 4000:3000 --name tel335-jwt -d tel335-jwt-docker
```

Ejecutar en el puerto 4000 dado que se especifica hacer el mapeo desde el puerto 4000 (externo) hacia el 3000 (interno)

# Remover contenedores

```
# docker container stop tel335-jwt
# docker container rm tel335-jwt
```