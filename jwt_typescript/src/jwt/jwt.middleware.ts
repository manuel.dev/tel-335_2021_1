import { Response, NextFunction } from 'express'
import jwt, { JwtPayload } from 'jsonwebtoken'
import { IRequest, IJWTPayload } from '../jwt/jwt.request.interface'
import { JWT_PRIVATE_KEY, RESPONSE_MESSAGES } from '../utils/constants'

interface IResponse {
    status: boolean,
    message?: string
}

export const jwtAPIMiddleware = (req: IRequest, res: Response, next: NextFunction): void => {
    const token = req.headers['access-token']
    if (token) {
        try {
            const decoded: (JwtPayload | string) = jwt.verify(token.toString(), JWT_PRIVATE_KEY)
            if (decoded) {
                req.decoded = decoded
                next()
            }
        } catch (error) {
            res.json(buildErrorResponse(RESPONSE_MESSAGES.JWT_INVALID_TOKEN))
        }
    } else {
        res.json(buildErrorResponse(RESPONSE_MESSAGES.JWT_EMPTY_TOKEN))
    }
}

function buildErrorResponse (message: string): IResponse {
    return {
        status: false,
        message
    }
}