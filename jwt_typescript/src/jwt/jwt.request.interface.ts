import { Request } from 'express'
import { JwtPayload } from 'jsonwebtoken';

export interface IJWTPayload extends Object {
    message: string
}

export interface IRequest extends Request {
    decoded: (JwtPayload | string)
}