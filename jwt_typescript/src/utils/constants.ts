import dotenv from 'dotenv'
dotenv.config()

export const PORT = process.env.PORT
export const API_KEY = process.env.API_KEY
export const API_SECRET = process.env.API_SECRET

export const RESPONSE_MESSAGES = {
    GENERIC_ERROR_TITLE: 'Error',
    JWT_INVALID_TOKEN: 'Invalid token',
    JWT_EMPTY_TOKEN: 'Empty token',
    JWT_TOKEN_OK: 'Correctly authenticated',
    JWT_TOKEN_NOK: 'There was a problem with your credentials. Please contact the administrator'
}

export const JWT_PRIVATE_KEY = process.env.JWT_PRIVATE_KEY