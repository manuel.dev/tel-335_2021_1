# MongoDB CRUD Template

Este repositorio contiene un template para comenzar a realizar un CRUD (Operaciones de Inserción, Actualización, Lectura y Borrado) sobre una base de datos NoSQL denominada MongoDB. Esta base de datos se levanta utilizando `docker-compose`.

## Instrucciones

Levante la base de datos y la api utilizando `docker-compose`:

```
docker-compose up -d
```

El proyecto está seteado para trabajar en el puerto 4000

Para remover los contenedores, ejecutar:

```
docker-compose down -v --rmi all --remove-orphans
```
