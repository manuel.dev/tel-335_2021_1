import User from '../model/User'

export const insertUserRepository = async (name: string, email: string) => {
    const user = new User({ name, email })
    await user.save()

    return {
        successful: true
    }
}

export const getUsersRepository = async () => {
    
}

export const getUserRepository = async (email: string) => {
    
}