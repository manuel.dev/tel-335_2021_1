import { insertUserRepository, getUsersRepository, getUserRepository } from '../repository/UserRepository'

export const insertUser = (email: string, name: string) => {
    return insertUserRepository(name, email)
}

export const getUsers = () => {
    return getUsersRepository()
}

export const getUser = (email: string) => {
    return getUserRepository(email)
}