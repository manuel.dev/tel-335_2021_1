exports.isPalindrome = (input) => {
    // Implementación del profesor
    /*if (!input) return false
    input = input.toLowerCase().replace(/[\W_]/g, '')
    const length = input.length
    for (let i = 0; i < length/2; i++) { // Solo necesitamos llegar a la mitad en el peor caso
        if (input[i] != input[length - 1 - i]) {
            return false
        }
    }
    return true*/

    // Juan Ardiles
    let valor = false
    if (!input) return false
    input = input.toLowerCase().replace(/[\W_]/g, '')
    for(let i=0; i<=input.length;i++){
        if(input[i] === input[input.length-i-1]){   // C espacial O(1), temp O(n)
            valor = true
        } else {
            return false
        }
    }
    if(valor){
        return true
    } else {
        return false
    }

    /*
    // Piero Oporto
    if (!input) return false
    input = input.toLowerCase()
    input = input.replace(/ /g, "")
    p_arr=[]
    for (i of input){
	    p_arr.push(i)    // Complejidad espacial O(n)
    }
    reves = p_arr.reverse()
    goal = input.length
    flag = 0
    for(i=0;i<input.length;i++){
        if (reves[i] == input[i]){  // Complejidad temporal O(n)
            flag++
        }
    }
    if(flag == goal){
        return true
    } else {
        return false 
    }*/

    /*
    // Sofía Castro
    if (!input) return false
    input = input.toLowerCase().replace(/[\W_]/g, '')
    let temp_length= input.length
    let array_input = input.split()
    let verification=0 
    let ver=false
	for(let index=0; index < input.length; index++){
		if ( array_input[index] == array_input[temp_length-1]){
			verification++
        }
		temp_length--
    }
    if (verification==array_input.length){
        ver=true
        return true
    } else {
        return false
    }*/

    /*
    // Vicente Alvear
    let i
	let aux = 0
	let x
    if (!input) return false
    input = input.toLowerCase().replace(/[\W_]/g, '')  // C espacial O(1), temp O(n)
	for (i = 0; i < input.length; i++){
		if (aux == 0){
			if (input[input.length - i - 1] == input[i]){
				x = true
            } else {
		        aux = aux + 1;
                x = false 
		    } 
  		}
    }
	return x*/
}