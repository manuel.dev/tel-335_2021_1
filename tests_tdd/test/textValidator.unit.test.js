const textValidator = require('../src/textValidator')
const palindrome = require('../src/palindrome')
const sinon = require('sinon')

describe('Text validator tests', () => {
    afterEach(() => {
        sinon.restore()
    })

    test('Si ingreso "oso" como input, debería sumar los números', () => {
        sinon.stub(palindrome, 'isPalindrome').withArgs('oso').returns(true)
        const result = textValidator.textValidate('oso', 2, 3)
        expect(result).toBe(5)
    })

    test('Si ingreso "perro" como input, debería restar los números', () => {
        sinon.stub(palindrome, 'isPalindrome').returns(false)
        const result = textValidator.textValidate('perro', 5, 4)
        expect(result).toBe(1)
    })

    test('Si ingreso "ana" como input, debería sumar los números', () => {
        sinon.stub(palindrome, 'isPalindrome').returns(true)
        const result = textValidator.textValidate('ana', '5', 4)
        expect(result).toBe(9)
    })

    test('Si ingreso "ana" como input, debería sumar los números', () => {
        sinon.stub(palindrome, 'isPalindrome').returns(true)
        const result = textValidator.textValidate('ana', 'hola', 4)
        expect(result).toBe(0)
    })
})