package com.example.elo329app.Model;

public class UserModel {
    private String userEmail;
    private String userPassword;

    public UserModel (String userEmail, String userPassword) {
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }

    public boolean validateCredentials (String inputEmail, String inputPass) {
        return userEmail.equals(inputEmail) && userPassword.equals(inputPass);
    }
}
