package com.example.elo329app.Login;

import android.content.Context;
import android.content.Intent;

import com.example.elo329app.Home.HomeActivity;

public class LoginRouter {
    public static void goToHome(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
    }
}
