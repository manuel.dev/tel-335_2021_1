import express from 'express'
import { createConnection } from 'typeorm'

import { insertUser, getUsers, getUser } from './controller/UserController'

const app = express()

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

createConnection().then(
    () => console.log('Conexión creada')
).catch(
    error => console.log('Error en la conexión', error)
)

app.get('/', (req, res) => {
    res.json({ message: 'App status OK' })
})

app.post('/insert', async (req, res) => {
    const response = await insertUser(req.body.email, req.body.name)
    res.json(response)
})

app.get('/users', async (req, res) => {
    const response = await getUsers()
    res.json(response)
})

app.get('/users/:email', async (req, res) => {
    const response = await getUser(req.params.email)
    res.json(response)
})

app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000')
})