import "reflect-metadata"
import { User } from '../model/User'
import { getRepository } from "typeorm"

export const insertUserRepository = async (name: string, email: string) => {
    const user = new User()

    user.email = email
    user.nombre = name

    await getRepository(User).save(user)

    return {
        successfull: true,
        message: 'User inserted correctly'
    }
}

export const getUsersRepository = async () => {
    const users = await getRepository(User).find()
    if (!users) {
        return {
            successfull: false,
            message: 'There are not users registered'
        }
    }

    return {
        successfull: true,
        users
    }
}

export const getUserRepository = async (email: string) => {
    const user = await getRepository(User).findOne({ email })
    if (!user) {
        return {
            successfull: false,
            message: 'User not registered'
        }
    }

    return {
        successfull: true,
        user
    }
}